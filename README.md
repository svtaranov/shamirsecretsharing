# ShamirSecretSharing

## Запуск программы

Для запуска программы необходимы файлы `ShamirSS.js`, `ShamirSS.wasm`, `testform.html`, которые находятся в корне проекта.  
Для запуска веб-формы `testform.html` можно воспользоваться встроенным инструментом `emrun` в [emscripten](https://emscripten.org/docs/getting_started/downloads.html). Обычно веб-браузеры не могут запустить сгенерированный файл .html через URL-адрес file:// (например, двойным щелчком по файлу) из-за правил CORS браузера по умолчанию. Возможный вариант запуска:  
- Устанавливаем [emscripten](https://emscripten.org/docs/getting_started/downloads.html). 
- Скачиваем файлы `ShamirSS.js`, `ShamirSS.wasm`, `testform.html` из корня проекта. 
- В консоли в каталоге, в который были закачены файлы, запускаем команду `emrun testform.html`


## Сборка проекта из исходников

Для сборки проекта из исходников необходимо установить [emscripten](https://emscripten.org/docs/getting_started/downloads.html).  

Краткая выдержка для установки [emscripten](https://emscripten.org/docs/getting_started/downloads.html):
```
# Get the emsdk repo
git clone https://github.com/emscripten-core/emsdk.git

# Enter that directory
cd emsdk

# Fetch the latest version of the emsdk (not needed the first time you clone)
git pull

# Download and install the latest SDK tools.
./emsdk install latest

# Make the "latest" SDK "active" for the current user. (writes .emscripten file)
./emsdk activate latest

# Activate PATH and other environment variables in the current terminal
source ./emsdk_env.sh
```

Процесс сборки проекта:  
```
# Скачиваем содержимое каталога `build` из файлов проекта.
# Переходим в каталог с заголовочными файлами и собираем объектные файлы библиотек:

cd ./build/src

emcc shamir.cpp GF\(256\).cpp -c

# Возвращаемся в каталог с главным файлом `ShamirSS.cpp` 

cd ..

# Собираем весь проект командой;

emcc ShamirSS.cpp ./src/shamir.o ./src/GF\(256\).o -o ShamirSS.js -sEXPORTED_FUNCTIONS=_secretsplit,_secretcombining -sEXPORTED_RUNTIME_METHODS=ccall,cwrap

```

Получившиеся после сборки файлы `ShamirSS.js`, `ShamirSS.wasm`, а также html форма `testform.html` являются минимально достаточными для запуска программы.

## Примеры работы программы
Данный пример был собран и выполнен на системе Ubuntu 22.04 LTS, emscripten gcc версии 3.1.55, браузер Mozilla Firefox 123.0.1.

После запуска команды `emrun testform.html` откроется браузер. Вывод программа производит в консоль браузера, в Mozilla Firefox, чтобы открыть консоль, нужно нажать правой кнопкой по пустому месту на странице, `Inspect(Q)`, вкладка `Console`.

Для примера, выполним разделение секретной фразы `habit reform correct dose stand nurse wise elevator client fashion physical hand phrase generated punch shock entire north file identify mnemonic words access numbers` с помощью схемы N=11 (всего частей), K=5 (порог).

Скриншот с примером заполнения веб-формы `testform.html` ![example_split.jpg](./pic/example_split.jpg).

Получим 11 сгенерированных частей:
```
Shard 1: 11_5_1_e50a4ad3615b4110958522b52085b690341e001fc88a31bdaf31da2f2a88e408f9e4a51e473a5be142dd894058b7ef1e4b12bff29bffa1b690d1d6d0b2885a98dac9e9a38319ebe89f419e2d8b46bc45875aa0dfec3e87255a89f6e39c078bce1a00465d2c58327e374cc8ce4a58b01f80b1b2a06ec93e0606afe74d83aa6b86daf084bc28482eb0a09562427e0681308d7c894e37d295816b38eeedb33c75c78e3de92bb6bb
Shard 2: 11_5_2_bf0465fc35ed71ec96557f36316f333ef91926582acfdec79a0f7cc6dd2cfcec16306fcd51dd91c59c045e98b05f4927e45d5e4ab4b9273dd111e5454a237a7c370c3dc96eff4f2b66d2424f8ed75329af96d1a678530ef9c49355e2267b1136af8d45aa5342d7f2c03f9703d212a91f27a214ca1464e1c81cb7cf399f9ec2a0544dbb4fa32fa3c255f809a2706c14230370712dead15e392defb4c661f052a84ddea29bdc2f
Shard 3: 11_5_3_533e7fc5290b31ab3c1a325aa5f8d48da22a2a2cb5e68279fae6cf1903164a344f3280fb0ceb182633572d7c72a184eb5f00108c4638f9ea7ec0a335d266ad29b72043a905f418963fd9c663f47739dfc048f522ec39cb3794415c6a8562bc56992c36ebceda3e441250a13c33facff5a6ddc8a04dd8f237e7e684676c4573a1b3caaa791cf0f1145da4cfa3032228cb05d222cda384ac720db40791db957a313a9931aa81eb
Shard 4: 11_5_4_613024a41be0a4fab558b7a81b1bbd79de8d9766ea8b2845575ab0235d8e3cbbdb218cf0343c6c58ab10491b49ac883a9f2ba983e82d3395f80e2034a2962c0e5987e4d77792d7bcf360dd7b529635d412e3b3a0833fbc292e9a245563466920229437276b4f07ef95ac6f969466bd7d2244ea1a860ef0dbbc83eac1713545fb8ad454d000e65f106f8da94ba836b16be74aa3dc066d4afc2457143389daaef9b501ce416e47
Shard 5: 11_5_5_b1a6a03938f8a6238b41a9e54ec0e066d65d8d4f2bc17ab04c6d454d45960885def7cb5c0fceb3b1f3e40a8e7f96f2c2959643292cd28f5efb885b20d0c4760a5237b283b193dbb5fcba00b0ca8e5a3133c725fb58a08ca066f2f7f738396107f79f8462dd93daf78829837a0353ef7bf3636210baf336b8d51d28cfcdbde4a2da2aa36c20f54b9bf2802fb8e08d82b94f249e4d6ba7d417d20c1d4f4e69eef04b231516d1a7
Shard 6: 11_5_6_93eba84512a912f8bb3d5224c6b20a8bbd8787b27542895c8ff46fdd25760fb689904aa0d5bad581d868bdb464ed21934302f1496f68cdedf9e6127de741574cb2d136811d61c905a9906e071074bf7b87e42b27523cf4f2c887fba26989aa719f5d1c9df401573cfa95730a77b89eb8f4c06cba0adc5855f080091b4f2f6d3421654b908e114a53364fc4a7981609de86abbaccfe81c7f423db2832661219a4810ace6a647b
Shard 7: 11_5_7_222c1e5b382c6313dc8151dd071869c5a81ff284c3cfd9d93e3b8037a8b20d7842d535579164af031e7724e0a4fb0fd8cd9f98f7a1856722ab14d9afdeade8ec8cead7665b0b0a300361c5ed11eb68496e3451559084e3b0edd1b90e7f89f09d02d7eab19d7e39cc715b0e74201d7f255040f81f213adcbd10884760eaba73ca25884cc84dfc44c723e68872b08ae9bdc63b2a9c9fa84df5ff801f91db8b249df33f0d424297
Shard 8: 11_5_8_95a5b3cd8643e6a84f789fe12e0a5e7a2c33e72c1baf5487c54aa545d7d8a876a8991d7b1012478e887cc6cef217c4887ad3b0b2b656844d96329dce9c983ae77a46da4172791cf2fb46e7d18d9a06c5975e27526dd1675857010a6301c8cd45bf9883fd2d29f6dd9918a534102e323cdcce6ba86be8e3415541c14f335c929352fd9d653c722da8b6123ee3f0723d1725d47a9fce99f00e1a88e366164eae8037a89bea3b0c
Shard 9: 11_5_9_e848d5d16fedc693dad198bd07c7591e3de9dc2890d6327ef80e0feca9b616eb54755c48b135282e486eaf343bf8b727b703dbcb9626dfeeeed09ff3ca3f0a49d5a7b3421b31f395440f6a6dcb6bc0a813b111aec7657c5a0a33d74d222d90e9602698c5c3197c0dccb947f8f20f37d0736de0f6aa50cb735997143ffecddc4e0b96c9a92a4e660e54b529210fe2eea6459527611b65bd1965d3b0423974405fde772b275a54
Shard 10: 11_5_10_9c69a37b6cbb76368d9bd47ea1f54e9fe1604726425385f0e84e7c44c52be70a2cd2ec97ecee7807024ba9c43e2ca29a59bbbf359ace47d580545c8ac8f7451ba59c53ac07daec9c2764aad49ecfa4f244c85877f23bc68d71bb5fdf8a1a511a5b76f267f1711d09d430336a6e42aad8bcbc7b25e204b943e560062343e83aaa36a9f31926bf3e352b6e65de03bbfd69430f3e2a0de13071f70478243c6c743127a6eddc600b
Shard 11: 11_5_11_80d5f7e48c88253f4197ce961c4977aaedf2133dbeede1797ff2cc692e996f671ead95ff32e5b2cc5cb21aff01ef8586104b5758b0ac0a72a9c2ee71d5ee90113ff68d1ceef9ccc73d06514941b9da4808d382a241a8fa4441b713ffe4805e7dcc49ac36c0e2243117da40344cf31caf66b8ecd434a7d3fa60205f269764cbd03bd15731d37d6f8c416db83a9c0cfd69cbb0ce15d4fe698ca25f15df697ce7de426e456e985f

```

Выберем случайные 6 частей и попробуем восстановить секрет. Для этого необходимо в последней текстовой формы через пробел указать части, из которых будет собираться исходный секрет. Пример заполнения веб-формы `testform.html` для сборки секретной фразы ![example_combine6.jpg](./pic/example_combine6.jpg).  

Попробуем собрать секретную фразу из 5 случайных частей. ![example_combine5.jpg](./pic/example_combine5.jpg).

Попробуем собрать секретную фразу из менее 5 частей. Программа выводит ошибку и пустую строку. ![example_error.jpg](./pic/example_combine_error.jpg).
 